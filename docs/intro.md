# Réseau Linux

## Adressage

### Command IP
> Temporaire !

```Bash
# afficher la configuration IP
ip a
# afficher la passerelle
ip r 
# Configurer son adresse ip sur l'interface ens33 
ip a add 192.168.1.25/24 dev ens33 
# effacer la configuration
ip a flush ens33
```
### Fichier interfaces.d
> Permanent

> nano /etc/network/interfaces

```Bash
# ne pas toucher à auto lo
# Configurer son adresse ip sur l'interface ens33
allow-hotplug eth0
iface eth0 inet dhcp
# ou 
auto ens33 
iface ens33 inet static 
    address 192.168.1.25/24
    gateway 192.168.1.254

auto ens33
iface ens33 inet static 
    address 192.168.1.25
    netmask 255.255.255.0
    gateway 192.168.1.254
# ajouter des adresse IP à la même interface
    up   ip addr add 172.16.139.21/24 dev eth0 label eth0:0
    down ip addr del 172.16.139.21/24 dev eth0 label eth0:0
   
```
> **Ne pas oublier de redémarrer l'interface**

``` 
systemctl restart networking 
```

### DNS
> nano /etc/resolv.conf

> ce fichier n'existe pas à la base / pas de redémarrage nécessaire
```Bash
# ajouter la config DNS
nameserver 192.168.1.254
```
>Test :
ping "IP passerelle" ou ping www.google.fr

### Serveur DNS
1. avec apt : bind9
>Fichiers de configuration

> **dig axfr nom.local** permet de récupérer toutes les infos dns

```bash
/etc/bind/named.conf *
# possibilité d'y ajouter des ACL (liste des réseaux internes)
acl mesrzo { 172.0.0.0/8; 192.168.10.0/24; 192.168.30.0/24 };
```

il est découpé en plusieurs fichiers inclus :

include /etc/bind/named.conf.options ; *Configuration des options*
```bash
/etc/bind/named.conf.options
# forwarder --> IP du serveur DNSrndc
forward only;
forwarders {  192.168.1.254; };
# restrictions aux hôtes auxquels répond le serveur voir named.conf ACL
allow-querry { mesrzo; };
# restrictions aux hôtes auxquels le serveur autorise des requêtes récursives voir named.conf ACL
allow-recursion { mesrzo; };
# DNSSEC non activé
dnssec-validation no;
#info version non communiqué
version none;
```
include /etc/bind/named.conf.local ; *Configuration des zones hébergées localement*

include /etc/bind/named.conf.default ; *zones Configuration des zones par défaut*

Commandes
```bash
named-checkconf # tester les configurations
named-checkzone # tester les configurations de zones
rndc reload #relancer le service bind9
rndc flush #vider le cache dns
```
>vérifier Fichier journal

tail -100 /var/log/syslog

>test avec infos : dig yahoo.fr

>si IPV6 actif
```bash
nano /etc/sysctl.conf
#ajouter
net.ipv6.conf.all.disable_ipv6 = 1

# après modif penser à valider la config
sysctl -p
```
2. avec apt dnsutils
DIG

### Zone DNS

>dans /etc/bind/
```bash
## dans /etc/bind/named.conf.local
# ajouter ses zones dns avec la ref du fichier db de zone
# dans le serveur DNS master
zone "nom.local" {
    type master ;
    file "db.nom.local" ;
    allow-transfer { IP.server.dns.secondaire; }; # si serveur dns secondaire
    allow-update { none; }; # option
};
# dans serveur DNS secondaire
zone "nom.local" {
    type slave ;
    masters { IP.server.DNS.master; };
    file "db.nom.local" ;
};

named-checkconf # tester les configurations
        
dig @IP.serveur.DNS.secondaire SOA nom.local # tester si le DNS secondaire fonctionne
```

```bash
## dans le dossier "directory : /var/cache/bind/" defini dans "named.conf.options" créer le fichier db.nom.local citer ci-dessus
# def des variables
$ORIGIN nom.local.
$TTL 86400
# def du SOA
@     IN     SOA    dns1.nom.local.     hostmaster.nom.local. (
                    2021121501 ; serial
                    21600      ; refresh after 6 hours
                    3600       ; retry after 1 hour
                    604800     ; expire after 1 week
                    86400 )    ; minimum TTL of 1 day

# def des serveurs DNS
      IN     NS     dns1.nom.local.
      IN     NS     dns2.nom.local.
# def des enregistrements
      IN     MX     10     mail.nom.local.
      IN     MX     20     mail2.nom.local.

             IN     A       10.0.1.5

server1      IN     A       10.0.1.5
server2      IN     A       10.0.1.7
dns1         IN     A       10.0.1.2 # ne pas oublier
dns2         IN     A       10.0.1.3 # ne pas oublier

ftp          IN     CNAME   server1
mail         IN     CNAME   server1
mail2        IN     CNAME   server2
www          IN     CNAME   server2
```
```bash
## tester les configurations de zones
named-checkzone nomdelazone /chemin/nomdufichier  

## relancer le service à chaque intervention
systemctl restart bind9
rndc reload

## tester les résolutions
ping ftp.nom.local
dig NS nom.local
dig SOA nom.local
```
### DNS Inverse

```bash
## dans /etc/bind/named.conf.local
# ajouter ses zones dns avec la ref du fichier db de zone
zone "1.0.10.in-addr.arpa" IN {
  type master;
  file "db.inv.10.0.1";
  allow-update { none; };
};

```
```bash
## dans le dossier "directory : /var/cache/bind/" defini dans "named.conf.options" créer le fichier db.inv.10.0.1 citer ci-dessus
# def des variables
$ORIGIN 1.0.10.in-addr.arpa.
$TTL 86400
@     IN     SOA    dns1.nom.local.     hostmaster.nom.local. (
                    2001062501 ; serial
                    21600      ; refresh after 6 hours
                    3600       ; retry after 1 hour
                    604800     ; expire after 1 week
                    86400 )    ; minimum TTL of 1 day

      IN     NS     dns1.nom.local.
      IN     NS     dns2.nom.local.

20    IN     PTR    alice.nom.local.
21    IN     PTR    betty.nom.local.
22    IN     PTR    charlie.nom.local.
23    IN     PTR    doug.nom.local.
24    IN     PTR    ernest.nom.local.
25    IN     PTR    fanny.nom.local.
```
```bash
## tester les configurations de zones
named-checkzone 

## relancer le service
rndc reload

## tester les résolutions
dig -x 192.168.10.2
```
### Nom de la machine
> 2 fichiers

> nano /etc/hosts --> 127.0.1.1 *modifier le nom*
>
> nano /etc/hostname --> *modifier le nom*

```bash
# afficher son nom
hostname
```
> il faut redémarrer la machine **reboot**

>test : ping *nom de la machine*

### Ordre des résolutions DNS
/etc/nsswitch.conf


## Routage

### IP route
> Dynamique et Temporaire !
```Bash
# afficher les routes
ip route
ip r
# Ajouter des routes
ip route add 10.11.12.3 via 172.16.6.123 # hôtes
ip route add 10.56.0.0/16 via 172.16.6.253 # Réseaux
ip route add default via 172.16.6.1 # route par défaut
# changer
ip route change default via 172.16.6.200
# supprimer
ip route del 10.56.0.0/16
```
Permanent soit dans /etc/network/interfaces soit par export ip route
```bash
# static route
up route add -net 172.17.250.0/24 gw 172.17.10.141 dev eth0
```
### Linux en Routeur

> Par défaut linux ne route pas donc il faut l'activer
```bash
# activer
nano /etc/sysctl.conf
net.ipv4.ip_forward=1
# lancer
sysctl -p
# verifier
sysctl net.ipv4.ip_forward
net.ipv4.ip_forward = 0 # 0 inactif / 1 actif
```

### NAT

>SNAT source ou DNAT destination

### DHCP
* DHCP DISCOVER : le client broadcast sur le réseau pour demander une configuration IP
* DHCP OFFER : le serveur DHCP fait une proposition de configuration (toujours en broadcast)
* DHCP REQUEST : le client accepte l'offre
* DHCP ACK : le serveur prend acte de l'acceptation et envoi le bail

>2 apt possibles : isc-dhcp-server ou kea

Nom du paquet  isc-dhcp-server
>Fichiers de configuration
```bash
/etc/default/isc-dhcp-server
INTERFACESv4="ens33" # si plusieurs interfaces il faut en ajouter
INTERFACESv6=""

/etc/dhcp/dhcpd.conf

##### Option générale par défaut #####

### RÉSEAU #####

## Nom du serveur DHCP
server-name "dns.nom.local";

## Mode autoritaire (autoritaire)
authoritative;

## Masque de sous-réseau
option subnet-mask 255.255.255.0;

### DOMAINE ###

## Nom du domaine 
option domain-name "nom.local"; #(def dans les zones DNS !!!!)

## Adresse IP du serveur DNS
# a remplacer par l ip de votre serveur dns ou par celle de votre fai
option domain-name-servers XXX.XXX.XXX.XXX;

## Type de mise à jour du DNS (aucune)
ddns-update-style none;

### TEMPS DE RENOUVÈLEMENT DES ADRESSES ###
default-lease-time 3600;
max-lease-time 7200;

### Sécurité ###

## refus(deny)/autorise(allow) les clients inconnus (refuse client inconnu)
deny unknown-clients;

## Use this to send dhcp log messages to a different log file (you also
## have to hack syslog.conf to complete the redirection).
log-facility local7;

##### RÉSEAUX #####
## déclaration sous réseau 192.168.1.*
subnet 192.168.1.0 netmask 255.255.255.0 {
  # Si vous voulez spécifier un domaine différent de celui par défaut :
  option domain-name "ubuntu-fr.lan";
  ## Adresse de diffusion 
  option broadcast-address 192.168.1.255;
  ## Serveurs DNS 
  option domain-name-servers 8.8.8.8, 1.1.1.1;
  ## routeur par défaut
  option routers 192.168.1.1;
        ## Plage d'attribution d'adresse
  	range 192.168.1.5 192.168.1.50;
  ## Option pxe nom du fichier servi.
  # elilo.efi pour ia64; pxelinux.0 pour x86
  # À placer à la racine du serveur TFTP.
  # Le fichier peut être spécifié dans la section « host », il deviendra alors prioritaire sur celui-ci 
  filename "pxelinux.0";
  # définit le serveur qui servira le fichier « pxelinux.0 »
  next-server 192.168.2.1;
  # évalue si l'adresse est déjà attribuée
  ping-check = 1;
}

## Déclaration sous réseau 192.168.2.*
subnet 192.168.2.0 netmask 255.255.255.0 {
  option domain-name "ubuntu-fr.lan";
  option broadcast-address 192.168.2.255;
  option domain-name-servers 8.8.8.8, 1.1.1.1;
  option routers 192.168.2.1;
  	range 192.168.2.2 192.168.2.100;
  next-server 192.168.2.1;
}

#### Configuration des hôtes avec IP fixée ####
# hôte « FTP »
host ftp {
  hardware ethernet 00:0f:75:af:eb:44;
  fixed-address 192.168.1.2;
  ### PXE ###
  # fichier spécifique à une machine 
  # filename "debian-installer/ia64/elilo.efi";
  # definit le serveur qui servira le fichier pxelinux.0
  # next-server 192.168.2.1;
}
# hôte « WEB »
host web {
  hardware ethernet 00:02:0d:31:d1:cc;
  fixed-address 192.168.1.3;
}
# hôte « mail »
host mail {
  hardware ethernet 00:02:55:d2:d1:cc;
  fixed-address 192.168.1.4;
}
# hôte « PORTABLE »
host portable {
  hardware ethernet 00:0e:af:31:d1:cc;
  fixed-address 192.168.2.2;
}
}
```
> Fichier de baux
```
/var/lib/dhcp/dhcpd.leases
```
>Fichier journal /var/log/syslog

>test config : dhcpd -t

>redémarrage du service :
```
systemctl (status/logstop/start/restart) isc-dhcp-server
```

>Mode debug du serveur DHCP 
```bash
## serveur DHCP
# arrêt du service DHCP
systemctl stop isc-dhcp-server.service  
# démarrage mode debug
dhcpd -d
```
### relai DHCP
Nom du paquet  isc-dhcp-relay
```
/etc/default/isc-dhcp-relay
SERVER="172.168.30.10"
INTERFACES="ens33 ens36"
```
>Mode debug du relay DHCP 
```bash
## Relay DHCP
# arrêt du service relay DHCP
systemctl stop isc-dhcp-relay.service  
# démarrage mode debug avec -iu (interface vers server DHCP) -id (interface vers clients DHCP) IP du DHCP
dhcrelay -d -iu ens37 -id ens36 192.168.10.12 
```
## SSH SCP

### SSH
> installer openSSH-server

tests d'installation : 
``` 
systemctl status sshd.service
ss -tpnl 
```

>sur windows puTTy ou mobaXterm

>plusieurs algorithmes de cryptage des clés : **RSA**, DSA, ECDSA

```bash
# générer une paire de clés
ssh-keygen -C "clé SSH client" -b 2048
# verification
ls -la .ssh/
```
On obtient une paire de fichiers :
* Une clé privée (à conserver en local et *backuper*)
* Une clé publique à installer sur les serveurs pour se connecter

Si le nom n'est pas précisé, les clés s’appellent :
dans /home/ID/.ssh/
* id_rsa pour la clé privée
* id_rsa.pub pour la clé publique

```bash
# Copier la clé publique dans .ssh destinataire (terminator pour plusieurs en même temps)
ssh-copy-id admin@192.168.6.66
```

Pour pouvoir se connecter avec une clé SSH en root :
```bash
# mise en place
ssh ID@192.168.6.66
su
cd /root
mkdir -m 700 .ssh
cp /home/ID/.ssh/authorized_keys /root/.ssh/
```

### SCP
> copie de fichier avec protocole SSH

> windows : WinSCP ou MobaXterm

```bash
scp /etc/eniconf.cfg user@ip:/tmp/
```

> rsync protocole assez similaire